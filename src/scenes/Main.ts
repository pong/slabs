import { Cameras } from "phaser";

export class Main extends Phaser.Scene {
    constructor() {
        super("main");
    }

    create() {     
    
        const zoom:number = 1.95;
        const horse = this.add.sprite(200, 200, 'sheet1', 'horse'); //menu
        const menu = this.add.sprite(this.sys.canvas.width / (2/zoom), this.sys.canvas.height / (2/zoom), 'menu', 'menu'); //menu
        this.cameras.main.setZoom(zoom);
    
        const music = this.sound.add('main'); //Main
        // music.play();
        
        this.input.on('pointerdown', (pointer: Phaser.Input.Pointer) => {
            music.stop();
            this.scene.start('levels');
        });
    }
}