import "phaser";
import { Preloader } from './scenes/Preloader';
import { Main } from './scenes/Main';
import { Levels } from './scenes/Levels';
    
var config = {
    type: Phaser.CANVAS,
    parent: "canvas",
    width: 1200,
    height: 820,
    pixelArt: true,
    backgroundColor: '#2d2d2d',
    scene: [
        Preloader,
        Main,
        Levels
    ]
};


const game = new Phaser.Game(config);